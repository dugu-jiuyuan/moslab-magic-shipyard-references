# 简介

STM32单片机是电子设计及其他相似类型的竞赛中最常用的主控制器。然而STM32单片机入门难度较大，学习曲线较为陡峭。为了降低STM32单片机学习难度，为实验室培养更多电子设计人才，实验室技术部整理了历年培训资料，并结合培训经验将STM32单片机教学整理为九个渐进的学习阶梯。同时，实验室技术部整理了相关驱动程序等软件资料与互联网的开资源资料，作为培训的参考。

---



# MOS单片机阶梯培训计划

包含两部分：

### 1.学习指南

STM32阶梯培训计划概述与学习指南。

### 2.PPT汇总

 STM32阶梯培训计划使用的PPT，汇总在了同一个PDF中。

---



# Enterprise系列培训用开发板资料

包括三部分：

### 1.开发板资料

开发板详细资料包括原理图、PCB设计图、BOM清单与Gerber文件。

### 2.基础示例程序

与培训方案配套的13组示例程序。

### 3.综合示例程序

综合性单片机项目程序示例。

------



# 资料获取

本培训方案的全部资料已上传至Gitee与百度网盘，可以自行前往下载。

### 1.单片机资料包：

主要包括硬件手册、调试程序、开源文档等与STM32单片机硬件直接相关的资源。

Gitee地址：[华北电力大学MOS实验室单片机培训-STM32资料库: 华北电力大学MOS实验室单片机培训资料STM32参考资料合集](https://gitee.com/dugu-jiuyuan/NCEPU-MOSLAB-SCM-STM32-reference-materials)

### 2.教学方案资料包：

Gitee地址：[https://gitee.com/dugu-jiuyuan/moslab-magic-shipyard-references](https://gitee.com/dugu-jiuyuan/moslab-magic-shipyard-references)

### 3.Keil MDK 网盘资源：

包括Keil MDK安装包，芯片包等文件。百度网盘下载地址：

[https://pan.baidu.com/s/1ErlthpDBWLjNBCUfriPQSQ?pwd=MOSR](https://pan.baidu.com/s/1ErlthpDBWLjNBCUfriPQSQ?pwd=MOSR)
