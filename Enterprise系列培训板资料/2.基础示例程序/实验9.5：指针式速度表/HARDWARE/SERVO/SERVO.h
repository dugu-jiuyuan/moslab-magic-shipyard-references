#ifndef __SERVO_H
#define __SERVO_H

#include "sys.h"
#include "Timer.h"

void Servo_Init(void);
void Servo_Drive(u8 angle);


#endif
