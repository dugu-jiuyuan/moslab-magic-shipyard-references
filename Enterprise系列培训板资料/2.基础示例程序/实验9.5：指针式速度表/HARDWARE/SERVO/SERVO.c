#include "sys.h"
#include "SERVO.h"
#include "TIMER.h"

void Servo_Init(void)
{
    TIM3_PWM_Init(1799,799);
    TIM3->CCR1=(45+0);
}


void Servo_Drive(u8 angle)
{
    TIM3->CCR1=(45+angle);
}
