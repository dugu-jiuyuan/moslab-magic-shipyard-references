#include "sys.h"
#include "usart.h"
#include "SERIAL.h"

SERIAL Serial2;

void USART2_Init(u32 bound)
{
    GPIO_InitTypeDef GPIO_InitStrue;
	USART_InitTypeDef USART_InitStrue;
	NVIC_InitTypeDef NVIC_InitStrue;
	
	// 外设使能时钟
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2 ,ENABLE );
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA,ENABLE);
	USART_DeInit(USART2);  //复位串口2 -> 可以没有
	
	// 初始化 串口对应IO口  TX-PA2  RX-PA3
	GPIO_InitStrue.GPIO_Mode=GPIO_Mode_AF_PP;
	GPIO_InitStrue.GPIO_Pin=GPIO_Pin_2;
	GPIO_InitStrue.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_Init(GPIOA,&GPIO_InitStrue);
	
	GPIO_InitStrue.GPIO_Mode=GPIO_Mode_IN_FLOATING;
	GPIO_InitStrue.GPIO_Pin=GPIO_Pin_3;
    GPIO_Init(GPIOA,&GPIO_InitStrue);
	
	// 初始化 串口模式状态
	USART_InitStrue.USART_BaudRate=bound; // 波特率
	USART_InitStrue.USART_HardwareFlowControl=USART_HardwareFlowControl_None; // 硬件流控制
	USART_InitStrue.USART_Mode=USART_Mode_Tx|USART_Mode_Rx; // 发送 接收 模式都使用
	USART_InitStrue.USART_Parity=USART_Parity_No; // 没有奇偶校验
	USART_InitStrue.USART_StopBits=USART_StopBits_1; // 一位停止位
	USART_InitStrue.USART_WordLength=USART_WordLength_8b; // 每次发送数据宽度为8位
	USART_Init(USART2,&USART_InitStrue);
	
	// 初始化 中断优先级
	NVIC_InitStrue.NVIC_IRQChannel=USART2_IRQn;
	NVIC_InitStrue.NVIC_IRQChannelCmd=ENABLE;
	NVIC_InitStrue.NVIC_IRQChannelPreemptionPriority=2;
	NVIC_InitStrue.NVIC_IRQChannelSubPriority=1;
	NVIC_Init(&NVIC_InitStrue);
    
//	USART_ITConfig(USART2,USART_IT_RXNE,ENABLE);//开启接收中断
	
    USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);//开启串口接受中断
	USART_Cmd(USART2,ENABLE);//使能串口


}

void USART2_IRQHandler(void)
{
    Serial_RX_u8str(&Serial2);
}



/************************************************************
本程序只供学习使用，未经作者许可，不得用于其它任何用途
修改日期:2023/2/24
版本：V4.1
作者：DG_JiuYuan
Gitee个人主页:https://gitee.com/dugu-jiuyuan
All rights reserved
************************************************************/


//将串口结构体与USART硬件绑定
void Serial_Init(SERIAL *Serial, USART_TypeDef* USARTx)
{
    Serial->USARTx=USARTx;
}

//用该串口结构体绑定的USART发送单个字符
void Serial_TX_u8(SERIAL *Serial, u8 c)
{
    USART_ClearFlag(Serial->USARTx, USART_FLAG_TC);//清除标志位，防止第一个字节丢失
    USART_SendData(Serial->USARTx, c);
    while(USART_GetFlagStatus(Serial->USARTx,USART_FLAG_TC)!=SET);
}

//用该串口结构体绑定的USART发送u8字符串
void Serial_TX_u8str(SERIAL *Serial, u8 *str)
{
    while(*str!='\0')
    {
        Serial_TX_u8(Serial, *str);
        str++;
    }
    Serial_TX_u8(Serial, '\r');
    Serial_TX_u8(Serial, '\n');    
}



//将收到的单个字符存入串口结构体的接收缓存
void Serial_RX_u8str(SERIAL *Serial)
{
    u8 Res;
    if(USART_GetITStatus(Serial->USARTx, USART_IT_RXNE) != RESET)  
    {
        Res =USART_ReceiveData(Serial->USARTx);	//读取接收到的数据
        // printf("%c",Res);

    #if (RX_MOOD==1)
        if(Res == '\n')
        {
            Serial->STA=1;
        }
    #elif (RX_MOOD==2)
        if(Res == '\r')
        {
            Serial->STA=1;//已经收到了'\r'
        }
        else if( Res == '\n' && Serial->STA==1)
        {
            Serial->STA=2;//已经收到了'\r'之后收到了'\n'
        }
    #endif
        else
        {
            Serial->STA=0;
            Serial->RX_BUF[Serial->RX_u8_num]=Res;
            Serial->RX_u8_num++;
        }
        
        //超长，重置零
        if(Serial->RX_u8_num>SERIAL_RX_LEN)
        {
            Serial->RX_u8_num=0;
            Serial->STA=0;
            
        }
        
    }
}

//检查是否接收到完整字符串，若接收到完整字符串，则返回字符串长度，否则返回0；
u16 Serial_RXcheck(SERIAL *Serial)
{
    u16 flag;
    if(Serial->STA==RX_MOOD&&Serial->RX_u8_num!=0)
    {
        Serial->RX_BUF[Serial->RX_u8_num] = '\0';//使接收缓存休止
        Serial->length = Serial->RX_u8_num;//记录接收到的字符串长度
        
        Serial->STA = 0;//重置接收状态
        Serial->RX_u8_num = 0;//重置计数器
        flag = 1;
    }
    else
    {
        flag = 0;
    }
    return flag;  

}

