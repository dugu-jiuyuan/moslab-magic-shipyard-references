#include "sys.h"
#include "delay.h"
#include "usart.h"
#include "LED.h"
#include "KEY.h"
#include "ADC.h"
#include "SERIAL.h"
#include "TIMER.h"
#include "_0_96OLED_Lib.h"
#include "_u8_Lib.h"
 

/************************************************************
本程序只供学习使用，未经作者许可，不得用于其它任何用途
修改日期:2023/2/24
版本：V4.1
作者：DG_JiuYuan
Gitee个人主页:https://gitee.com/dugu-jiuyuan
All rights reserved
************************************************************/


//显示器结构体中内置了一个大数组，一定要把显示器结构体放在主函数之外，以防堆栈溢出！！！
SCREEN Screen;
    

int main(void)
{
    u8 time[9];//存储实时时间的字符串
    
    delay_init();	    //延时函数初始化	
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
    uart_init(115200);
    
    LED0_Init();
    KEY_EXTI_Init();
    //使用外部中断控制秒表
    //KEY0：开始计时、停止计时并清零
    //KEY1：暂停计时、继续计时
    
    TIM2_Int_Init(7199,99);//周期10毫秒，初始化后关闭定时器中断
    
    Screen_Init(&Screen,'A',11,'A',12);//初始化显示器
    
    minute=0;second=0;m_second=0;//时间置零
    
    while(1)
    {
        TurnString((char*)time,"%02d:%02d.%02d",minute,second,m_second);//将数字合并至字符串time
        Screen_ShowString(&Screen,16,24,time,24);
        Screen_Refresh(&Screen);
        printf("%02d:%02d:%03d\r\n",minute,second,m_second);
      
    }

}

