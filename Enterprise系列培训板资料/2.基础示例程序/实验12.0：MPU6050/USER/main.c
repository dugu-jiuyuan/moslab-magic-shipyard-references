#include "sys.h"
#include "delay.h"
#include "usart.h"
#include "LED.h"
#include "KEY.h"
#include "ADC.h"
#include "TIMER.h"
#include "MPU6050.h"
#include "_0_96OLED_Lib.h"
#include "_u8_Lib.h"
#include "inv_mpu.h"
#include "inv_mpu_dmp_motion_driver.h" 
 

/************************************************************
本程序只供学习使用，未经作者许可，不得用于其它任何用途
修改日期:2023/2/24
版本：V4.1
作者：DG_JiuYuan
Gitee个人主页:https://gitee.com/dugu-jiuyuan
All rights reserved
************************************************************/


SCREEN Screen;//由于MPU6050的DMP功能会占用较大的堆栈，因此一定要把显示屏结构体扔在主函数之外

int main(void)
{
	float pitch,roll,yaw; 		//欧拉角
	short aacx,aacy,aacz;		//加速度传感器原始数据
	short gyrox,gyroy,gyroz;	//陀螺仪原始数据
	short temp;					//温度
    u8 pitch_str[16],roll_str[16],yaw_str[16];
    
    delay_init();	    //延时函数初始化	
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
    uart_init(115200);
    
    LED0_Init();
    Screen_Init(&Screen,'A',11,'A',12);
    MPU_Init();//初始化陀螺仪本体
    while(mpu_dmp_init())
 	{
		printf("MPU6050 DMP ERROR\r\n");
 		delay_ms(200);
	}  //等待DMP功能初始化
    
    
    printf("MPU6050 OK\r\n");
 	while(1)
	{
		if(mpu_dmp_get_data(&pitch,&roll,&yaw)==0)
		{ 
            MPU_Get_Accelerometer(&aacx,&aacy,&aacz);	//得到加速度传感器数据
			MPU_Get_Gyroscope(&gyrox,&gyroy,&gyroz);	//得到陀螺仪数据
            temp=MPU_Get_Temperature();//读取温度（扩大了100倍）
            
            printf("aacx=%d, aacy=%d, aacz=%d\r\n",aacx, aacy, aacz);
            printf("gyrox=%d, gyroy=%d, gyroz=%d\r\n",gyrox, gyroy, gyroz);
            printf("temperature=%d\r\n",temp);
            printf("pitch=%.2f, roll=%.2f, yaw=%.2f\r\n",pitch, roll, yaw);
            printf("***************************************************\r\n\r\n");
            
            TurnString((char*)pitch_str,"pitch=%.2f  ",pitch);
            TurnString((char*)yaw_str,"yaw=%.2f  ",yaw);
            TurnString((char*)roll_str,"roll=%.2f  ",roll);
            
            Screen_ShowString(&Screen,0,0,pitch_str,16);
            Screen_ShowString(&Screen,0,20,yaw_str,16);
            Screen_ShowString(&Screen,0,40,roll_str,16);
            
            Screen_Refresh(&Screen);
            
        }	

      
    }

}
