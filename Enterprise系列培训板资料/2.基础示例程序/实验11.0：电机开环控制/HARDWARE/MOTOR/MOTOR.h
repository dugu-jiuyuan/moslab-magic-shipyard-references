#ifndef __MOTOR_H
#define __MOTOR_H

#include "sys.h"

void Motor_Init(void);
void Motor_Drive(int speed);


#endif
