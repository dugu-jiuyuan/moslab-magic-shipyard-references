#include "MOTOR.h"
#include "sys.h"
#include "usart.h"

//将PB0，PB1引脚作为电机的控制引脚，这两个脚同时也是TIM3的通道3，4的引脚，但这里只做普通IO使用
void Motor_Init(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;
 
 	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB,ENABLE);//使能GPIOB时钟

	GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_0|GPIO_Pin_1;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    
 	GPIO_Init(GPIOB, &GPIO_InitStructure);
    
    GPIO_ResetBits(GPIOB, GPIO_Pin_0|GPIO_Pin_1);
    
}


void Motor_Drive(int speed)
{
    printf("speed = %d\r\n",speed);
    
    if(speed > 0)
    {
        //控制电机的引脚
        speed = -speed;
        PBout(0) = 1;
        PBout(1) = 0;
        
        //控制LED阵列，表示电机转动方向
        PBout(12) = 1;
        PBout(13) = 0;
        
    }
    else if(speed < 0)
    {
        //控制电机的引脚
        PBout(0) = 0;
        PBout(1) = 1;
        
        //控制LED阵列，表示电机转动方向
        PBout(12) = 0;
        PBout(13) = 1;
        
        //速度取绝对值
        speed = -speed;
        
    }
    else
    {
        //控制电机的引脚
        PBout(0) = 0;
        PBout(1) = 0;
        
        //控制LED阵列，表示电机转动方向
        PBout(12) = 0;
        PBout(13) = 0;
        
    }
    
    TIM3->CCR1 = speed;
    
}
