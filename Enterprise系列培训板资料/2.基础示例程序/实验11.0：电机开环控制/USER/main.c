#include "sys.h"
#include "delay.h"
#include "usart.h"
#include "LED.h"
#include "KEY.h"
#include "ADC.h"
#include "SERIAL.h"
#include "TIMER.h"
#include "MOTOR.h"
#include "SERVO.h"
 
/************************************************************
本程序只供学习使用，未经作者许可，不得用于其它任何用途
修改日期:2023/2/24
版本：V4.1
作者：DG_JiuYuan
Gitee个人主页:https://gitee.com/dugu-jiuyuan
All rights reserved
************************************************************/


int main(void)
{
    int speed,flag;
    delay_init();	    //延时函数初始化	
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
    uart_init(115200);
    
    LED0_Init();
    LEDGroup_Init();
    KEY_Init();

    TIM3_PWM_Init(1999,0);
    
    while(1)
    {
        flag=0;
        //speed = 800;
        if(KEY_Read(0)==1)
        {
            speed-=200;
            if(speed<-2000)
                speed=-2000;
            flag++;
        }
        else if(KEY_Read(1)==1)
        {
            speed+=200;
            if(speed>2000)
                speed=2000;
            flag++;
            
        }
        if(flag!=0)
        {
            Motor_Drive(speed);
            delay_ms(20);
            printf("speed=%d\r\n",speed);
            flag = 0;
            
        }
        LED0_Flash();
        
    }

}



