#include "sys.h"
#include "delay.h"
#include "usart.h"
#include "LED.h"
#include "KEY.h"
#include "ADC.h"
 
 
/************************************************************
本程序只供学习使用，未经作者许可，不得用于其它任何用途
修改日期:2023/2/24
版本：V4.1
作者：DG_JiuYuan
Gitee个人主页:https://gitee.com/dugu-jiuyuan
All rights reserved
************************************************************/


int main(void)
{
    u16 adc_ch0,adc_ch1;
    float U_0,U_1;
    delay_init();	    //延时函数初始化	
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
    uart_init(115200);
    
    LED0_Init();
    KEY_Init();
    ADC1_Init();
    
    while(1)
    {
        LED0_Flash();
        adc_ch0=Read_ADC(0);
        adc_ch1=Read_ADC(1);
        printf("ADC_CH0=%d     ADC_CH1=%d\r\n",adc_ch0,adc_ch1);//输出ADC的读取值
        
        //将ADC读取值转换为实际电压值
        U_0 = adc_ch0*3.3/4095.0;
        U_1 = adc_ch1*3.3/4095.0;
        printf("U_0=%.2fV    U_1=%.2fV\r\n",U_0,U_1);
        printf("***********************************\r\n");//输出转换后的电压
        
    }

}

