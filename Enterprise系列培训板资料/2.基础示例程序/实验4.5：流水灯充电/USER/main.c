#include "sys.h"
#include "delay.h"
#include "usart.h"
#include "LED.h"
#include "KEY.h"
#include "ADC.h"
 
 
/************************************************************
本程序只供学习使用，未经作者许可，不得用于其它任何用途
修改日期:2023/2/24
版本：V4.1
作者：DG_JiuYuan
Gitee个人主页:https://gitee.com/dugu-jiuyuan
All rights reserved
************************************************************/


int main(void)
{
    u16 adc_value;
    u8 flag=0;
    delay_init();	    //延时函数初始化	
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
    uart_init(115200);
    
    LED0_Init();
    KEY_Init();
    ADC1_Init();
    LEDGroup_Init();
    
    while(1)
    {
        if(KEY_Read(0)==1)
        {
            flag=0;
        }
        else if(KEY_Read(1)==1)
        {
            flag=1;
        }
        
        adc_value=Read_ADC(flag);
        printf("ADC_CH%d=%d,flag=%d\r\n",flag,adc_value,flag);
        
        GPIO_SetBits(GPIOB,GPIO_Pin_12|GPIO_Pin_13|GPIO_Pin_14|GPIO_Pin_15);
        if(adc_value>100)
            PBout(12)=0;
        if(adc_value>1000)
            PBout(13)=0;
        if(adc_value>2000)
            PBout(14)=0;
        if(adc_value>3000)
            PBout(15)=0;
            
        LED0_Flash();
        
    }

}

