#include "sys.h"
#include "delay.h"
#include "usart.h"
#include "LED.h"
#include "KEY.h"
#include "ADC.h"
#include "SERIAL.h"
 
 
/************************************************************
本程序只供学习使用，未经作者许可，不得用于其它任何用途
修改日期:2023/2/24
版本：V4.1
作者：DG_JiuYuan
Gitee个人主页:https://gitee.com/dugu-jiuyuan
All rights reserved
************************************************************/


int main(void)
{
    delay_init();	    //延时函数初始化	
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
    uart_init(115200);
    
    LED0_Init();
    USART2_Init(9600);//初始化USART2，波特率设置为9600
    
    Serial_Init(&Serial2,USART2);//将硬件USART2绑定到Serial2结构体
    
    while(1)
    {
        LED0_Flash();
        if(Serial_RXcheck(&Serial2)!=0)//检查是否接受到了完整的字符串
        {
            printf("%s",Serial2.RX_BUF);//将USART2接收到的字符串从USART1发送到上位机。
            Serial_TX_u8str(&Serial2,Serial2.RX_BUF);//将USART2收到的数据原路发回
        }
        else
        {
            Serial_TX_u8str(&Serial2,(u8*)"NO RECEIVE");//将USART2收到的数据原路发回
        }
            
        
    }

}

