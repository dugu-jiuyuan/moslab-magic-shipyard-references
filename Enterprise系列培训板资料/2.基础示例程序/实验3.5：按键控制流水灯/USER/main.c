#include "sys.h"
#include "delay.h"
#include "usart.h"
#include "LED.h"
#include "KEY.h"
 
 
/************************************************************
本程序只供学习使用，未经作者许可，不得用于其它任何用途
修改日期:2023/2/24
版本：V4.1
作者：DG_JiuYuan
Gitee个人主页:https://gitee.com/dugu-jiuyuan
All rights reserved
************************************************************/


int main(void)
{
    delay_init();	    //延时函数初始化	
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
    uart_init(115200);
    
    LED0_Init();
    LEDGroup_Init();
    KEY_Init();
    
    while(1)
    {     
        LED0_Flash();
        while((KEY_Read(0)!=1)&&(KEY_Read(1)!=1))
        {
            ;//等待按键按下
        }
        if(KEY_Read(0)==1)
        {
            LEDGroup_Flash(2);
        }
        else if(KEY_Read(1)==1)
        {
            LEDGroup_Flash(1);
        }
        
    }

}

