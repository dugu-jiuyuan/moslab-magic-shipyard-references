#include "sys.h"
#include "delay.h"
#include "usart.h"
#include "LED.h"
#include "KEY.h"
#include "ADC.h"
#include "SERIAL.h"
#include "TIMER.h"
#include "SERVO.h"
 

/************************************************************
本程序只供学习使用，未经作者许可，不得用于其它任何用途
修改日期:2023/2/24
版本：V4.1
作者：DG_JiuYuan
Gitee个人主页:https://gitee.com/dugu-jiuyuan
All rights reserved
************************************************************/


int main(void)
{
    int angle,flag;
    delay_init();	    //延时函数初始化	
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
    uart_init(115200);
    LED0_Init();
    KEY_Init();
    
    TIM3_PWM_Init(1799,799);
    
    while(1)
    {
        flag=0;
        if(KEY_Read(0)==1)
        {
            angle-=30;
            if(angle<0)
                angle=0;
            flag++;
        }
        else if(KEY_Read(1)==1)
        {
            angle+=30;
            if(angle>180)
                angle=180;
            flag++;
            
        }
        if(flag!=0)
        {
            Servo_Drive(angle);
            delay_ms(20);
            printf("angle=%d\r\n",angle);
            flag = 0;
        }
        LED0_Flash();
        
    }

}

