#ifndef __SERIAL_H
#define __SERIAL_H

#include "sys.h"

#define RX_MOOD 2
//RX_MOOD 1：接收以'\n'单换行符结尾
//RX_MOOD 2：接收以'\r\n'双换行符结尾，一般用这个


#define SERIAL_RX_LEN 64
//#define SERIAL_TX_LEN 64

typedef struct{
    USART_TypeDef* USARTx;
    u8 RX_BUF[SERIAL_RX_LEN];
    u8 usart_num;
    u8 flag;
    u16 STA;
    u16 RX_u8_num;
    u16 length;
}SERIAL;

extern SERIAL Serial2;

void USART2_Init(u32 bound);
void USART2_IRQHandler(void);

void Serial_Init(SERIAL *Serial, USART_TypeDef* USARTx);
void Serial_TX_u8(SERIAL *Serial, u8 c);
void Serial_TX_u8str(SERIAL *Serial, u8 *str);
void Serial_RX_u8str(SERIAL *Serial);
u16 Serial_RXcheck(SERIAL *Serial);


#endif
