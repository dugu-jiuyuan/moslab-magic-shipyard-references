#include "sys.h"
#include "delay.h"
#include "usart.h"
#include "LED.h"
#include "KEY.h"
#include "ADC.h"
#include "SERIAL.h"
#include "TIMER.h"


/************************************************************
本程序只供学习使用，未经作者许可，不得用于其它任何用途
修改日期:2023/2/24
版本：V4.1
作者：DG_JiuYuan
Gitee个人主页:https://gitee.com/dugu-jiuyuan
All rights reserved
************************************************************/


int main(void)
{
    delay_init();	    //延时函数初始化	
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
    uart_init(115200);
    
    LED0_Init();
    LEDGroup_Init();
    
    TIM4_Encoder_Init();
    TIM2_Int_Init(7199,100);//开启定时器2中断
    //在定时器2中断：TIM2_IRQHandler(void)
    //读取编码器的值并输出
    
    while(1)
    {
        LED0_Flash();
        
    }

}



