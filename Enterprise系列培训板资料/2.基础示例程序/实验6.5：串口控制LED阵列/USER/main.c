#include "sys.h"
#include "delay.h"
#include "usart.h"
#include "LED.h"
#include "KEY.h"
#include "ADC.h"
#include "SERIAL.h"
 
/************************************************************
本程序只供学习使用，未经作者许可，不得用于其它任何用途
修改日期:2023/2/24
版本：V4.1
作者：DG_JiuYuan
Gitee个人主页:https://gitee.com/dugu-jiuyuan
All rights reserved
************************************************************/


//！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！
//
//注意！！！！正点原子原版的USART1接收中断较为难用，推荐重写并使用MOSLAB开发的串口结构体处理方法。
//仅在本示例程序中，我们需要修改SYSTWM文件夹中的usart.c与usart,h文件。
//
//！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！


int main(void)
{
    u8 led;
    delay_init();	    //延时函数初始化	
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
    uart_init(115200);
    
    LED0_Init();
    LEDGroup_Init();
    Serial_Init(&Serial2,USART2);//将硬件USART2绑定到Serial2结构体
    
    while(1)
    {
        LED0_Flash();
        if(Serial_RXcheck(&Serial1)!=0)//检查是否接受到了完整的字符串
        {
            //GPIO_SetBits(GPIOB,GPIO_Pin_12|GPIO_Pin_13|GPIO_Pin_14|GPIO_Pin_15);
            if(Serial1.RX_BUF[0]>='1'&&Serial1.RX_BUF[0]<='4')
            {
                printf("LED%c IS CONTROLLED!\r\n",Serial1.RX_BUF[0]);//将USART1接收到的字符串从USART1发送到上位机。
                led=11+Serial1.RX_BUF[0]-'0';
                PBout(led)=!PBout(led);
            }
            Serial1.STA=0;//将USART1接收状态复位，覆盖接收缓存区。
        }
        
        
    }

}

