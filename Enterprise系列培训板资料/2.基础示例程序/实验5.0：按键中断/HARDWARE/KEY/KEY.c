#include "sys.h"
#include "KEY.h"
#include "delay.h"
#include "usart.h"

void KEY_Init(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;
 
 	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA,ENABLE);//使能GPIOA时钟

	GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_4|GPIO_Pin_5;//待配置GPIO为改组的第4、5号
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU; //将GPIO配置为上拉输入模式
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;//GPIO最大工作频率为50Mhz
    
 	GPIO_Init(GPIOA, &GPIO_InitStructure);//初始化GPIOA
    

}   


//读取按键，检查按键是否被按下。
u8 KEY_Read(u8 key)
{
    u8 key_value=0;//默认按键没被按下
    if(PAin((4+key))==0)//因为是上拉输入，结合原理图，读取到低电平时，按键被按下
    {
        delay_ms(10);//延时10毫秒消除按键抖动
        if(PAin((4+key))==0)//再次判定按键情况
            key_value=1;//如果按键被按下，则返回1
    }
    
    return key_value;//

}



void KEY_EXTI_Init(void)
{
 	NVIC_InitTypeDef NVIC_InitStructure;
    GPIO_InitTypeDef GPIO_InitStructure;
    EXTI_InitTypeDef EXTI_InitStructure;
 
 	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA,ENABLE);//使能PORTA,PORTE时钟
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO,ENABLE);	//使能复用功能时钟
    
	GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_4|GPIO_Pin_5;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU; 
 	GPIO_Init(GPIOA, &GPIO_InitStructure);
    

    //PA4 中断线以及中断初始化配置   下降沿触发
  	GPIO_EXTILineConfig(GPIO_PortSourceGPIOA,GPIO_PinSource4);

  	EXTI_InitStructure.EXTI_Line=EXTI_Line4;	
  	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;	
  	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling ;
  	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  	EXTI_Init(&EXTI_InitStructure);	 	//根据EXTI_InitStruct中指定的参数初始化外设EXTI寄存器

    
  	NVIC_InitStructure.NVIC_IRQChannel = EXTI4_IRQn;			//使能按键WK_UP所在的外部中断通道
  	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x02;	//抢占优先级2， 
  	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x02;					//子优先级3
  	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;								//使能外部中断通道
  	NVIC_Init(&NVIC_InitStructure); 
    
    
    //PA5 中断线以及中断初始化配置   下降沿触发
  	GPIO_EXTILineConfig(GPIO_PortSourceGPIOA,GPIO_PinSource5);

  	EXTI_InitStructure.EXTI_Line=EXTI_Line5;	
  	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;	
  	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling ;
  	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  	EXTI_Init(&EXTI_InitStructure);	 	//根据EXTI_InitStruct中指定的参数初始化外设EXTI寄存器

    
  	NVIC_InitStructure.NVIC_IRQChannel = EXTI9_5_IRQn;			//使能按键WK_UP所在的外部中断通道
  	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x02;	//抢占优先级2， 
  	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x02;					//子优先级3
  	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;								//使能外部中断通道
  	NVIC_Init(&NVIC_InitStructure); 
    
}

void EXTI4_IRQHandler(void)
{
	delay_ms(20);//消抖
	if(PAin(4)==0)	 //按键KEY0
	{
        printf("KEY0 IS PRESSED\r\n");
	}		 
	EXTI_ClearITPendingBit(EXTI_Line4);  //清除LINE4上的中断标志位
}


void EXTI9_5_IRQHandler(void)
{
	delay_ms(20);//消抖
    if(PAin(5)==0)
    {
        printf("KEY1 IS PRESSED\r\n");
    }
	EXTI_ClearITPendingBit(EXTI_Line5);  //清除LINE5上的中断标志位 
}


