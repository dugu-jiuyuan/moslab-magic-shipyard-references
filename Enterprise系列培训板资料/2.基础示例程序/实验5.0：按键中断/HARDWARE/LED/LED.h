#ifndef __LED_H
#define __LED_H

#include "sys.h"

void LED0_Init(void);
void LED0_Flash(void);

#endif
