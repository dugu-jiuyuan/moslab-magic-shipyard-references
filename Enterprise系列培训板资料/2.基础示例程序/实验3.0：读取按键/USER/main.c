#include "sys.h"
#include "delay.h"
#include "usart.h"
#include "LED.h"
#include "KEY.h"
 

/************************************************************
本程序只供学习使用，未经作者许可，不得用于其它任何用途
修改日期:2023/2/24
版本：V4.1
作者：DG_JiuYuan
Gitee个人主页:https://gitee.com/dugu-jiuyuan
All rights reserved
************************************************************/


int main(void)
{
    u8 key0,key1;      //存储按键读取结果
    delay_init();	    //延时函数初始化	
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
    uart_init(115200);
    
    LED0_Init();        //初始化LED0
    KEY_Init();         //初始化按键
    
    while(1)
    {     
        LED0_Flash();
        key0=KEY_Read(0);   //读取按键0
        key1=KEY_Read(1);   //读取按键1
        if(key0!=0 ||key1!=0 )
        {
            printf("key0=%d,key1=%d\r\n",key0,key1);
        }
        
    }

}

