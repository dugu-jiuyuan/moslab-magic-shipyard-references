#include "sys.h"
#include "LED.h"
#include "delay.h"


void LED0_Init(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;
 
 	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC,ENABLE);//使能GPIOC时钟

	GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_13;//待配置GPIO为改组的第13号
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; //将GPIO配置为推挽输出模式
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;//GPIO最大工作频率为50Mhz
    
 	GPIO_Init(GPIOC, &GPIO_InitStructure);//初始化GPIOC
    
    GPIO_SetBits(GPIOC,GPIO_Pin_13);//设置PC13默认输出电平为高
    
    //上一行等价与这条语句：PCout(13)=1;
    
}   

void LED0_Flash(void)
{
	PCout(13)=0;
	delay_ms(500);
	PCout(13)=1;
	delay_ms(500);
}


