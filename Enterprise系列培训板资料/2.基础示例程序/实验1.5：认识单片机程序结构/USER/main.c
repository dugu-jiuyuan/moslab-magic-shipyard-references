#include "sys.h"
#include "delay.h"
#include "usart.h"
#include "example.h"
  
  
/************************************************************
本程序只供学习使用，未经作者许可，不得用于其它任何用途
修改日期:2023/2/24
版本：V4.1
作者：DG_JiuYuan
Gitee个人主页:https://gitee.com/dugu-jiuyuan
All rights reserved
************************************************************/


int main(void)
{
    u8 i;//Keil MDK默认的C语言规范较为古旧，所有变量必须在调用第一个函数之前初始化完毕
    
    delay_init();	    //延时函数初始化	
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);//中断优先级分组
    uart_init(115200);//初始化串口1，可以使用printf函数了
    
    Student_Init(&StudentList[0], "DG_JiuYuan", 201, 20); 
    Student_Init(&StudentList[1], "OY_GaoXiang", 202, 20); 
    Student_Init(&StudentList[2], "WHY_Why", 203, 19); 
    
    printf("Init Complete\r\n");
    
    while(1)
    {
        for(i=0;i<3;i++)
        {
            printf("*********************************************\r\n");
            printf("name: %s\r\n",StudentList[i].name);
            printf("num: %d\r\n",StudentList[i].num);
            printf("age: %d\r\n",StudentList[i].age);
            printf("*********************************************\r\n\r\n");
            
            delay_ms(1000);  //延时1000ms
        }
        
    }

}

