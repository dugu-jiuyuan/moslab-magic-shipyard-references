#ifndef __EXAMPLE_H
#define __EXAMPLE_H

typedef struct{
    char name[20];
    int num;
    int age;
    
}STUDENT;

extern STUDENT StudentList[3];

void Student_Init(STUDENT *student, char *name, int num, int age);

#endif
