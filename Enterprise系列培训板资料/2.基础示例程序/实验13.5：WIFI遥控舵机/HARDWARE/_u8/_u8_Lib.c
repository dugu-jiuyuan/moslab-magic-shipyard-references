#include "_u8_Lib.h"
#include "sys.h"
#include "stdarg.h"
#include "stdio.h"


void u8cpy(u8 *str1,u8 *str2)
{
	while(*str2!='\0')
	{
		*str1 = *str2;
		str1++;
		str2++;
	}
	*str1 = '\0';

}


u32 u8len(u8 *str)
{
	u32 num = 0;
	while(*str!='\0')
	{
		str++;
		num++;
	}
	
	return num;
	
}


u8 Get_numlen(u32 num)
{
	u8 len = 0;
	while(num!=0)
	{
		num/=10;
		len++;
	}
	
	return len;
}


u32 u8pow(u8 m, u8 n)
{
	u32 result=1;
	while(n--)
	{
	  result*=m;
	}
	return result;
}

void TurnString(char* Final,char* fmt,...)
{
	va_list ap;
    va_start(ap,fmt);
    vsprintf((char*)Final,fmt,ap);
    va_end(ap);
}
