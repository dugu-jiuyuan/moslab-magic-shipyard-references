#ifndef __ESP01S_H
#define __ESP01S_H

#include "sys.h"
#include "SERIAL.h"

extern SERIAL ESP01S;

void ESP01S_Init(void);
void ESP01S_Send(u8 *str);
void ESP01S_Connect(void);
void ESP01S_DisConnect(void);

#endif
