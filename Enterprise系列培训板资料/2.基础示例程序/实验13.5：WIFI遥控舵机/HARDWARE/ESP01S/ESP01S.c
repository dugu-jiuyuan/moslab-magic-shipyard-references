#include "sys.h"
#include "ESP01S.h"
#include "SERIAL.h"
#include "_u8_Lib.h"
#include "usart.h"
#include "delay.h"

SERIAL ESP01S;


void ESP01S_Init(void)
{
    //根据AT指令手册连接WIFI
    u8 AT1[]="AT+CWMODE=3";
    //u8 AT2[]="AT+CWJAP=DG_JiuYuan\",\"12345678\"";//输入自己的WIFI名和密码
    
    USART2_Init(115200);
    
    Serial_Init(&ESP01S, USART2);
    Serial_TX_u8str(&ESP01S, AT1);
    delay_ms(1000);
    //Serial_TX_u8str(&ESP01S, AT2);
    
}

void ESP01S_Send(u8 *str)
{
    u16 len;
    u8 AT_TX[16];
    len = u8len(str);
    TurnString((char*)AT_TX,"AT+CIPSEND=%d\0",len+2);
    
    Serial_TX_u8str(&ESP01S,AT_TX);
    delay_ms(10);
    Serial_TX_u8str(&ESP01S,str);
    Serial_RXcheck(&ESP01S);
    
    
}


void ESP01S_Connect(void)
{
    u8 AT_C[]="AT+CIPSTART=\"TCP\",\"192.168.43.1\",8086";
    u8 AT_S[]="TCP Server has been open.";
    Serial_TX_u8str(&ESP01S, AT_C); 
    PCout(13)=0;
    ESP01S_Send(AT_S);
    delay_ms(1000);
    Serial_RXcheck(&ESP01S);
    PCout(13)=1;
    
}


void ESP01S_DisConnect(void)
{
    u8 AT_DC[]="AT+CIPCLOSE";
    u8 AT_S[]="TCP Server will be closed.";
    ESP01S_Send(AT_S);
    delay_ms(100);
    Serial_TX_u8str(&ESP01S, AT_DC);
    PCout(13)=0;
    delay_ms(1000);
    Serial_RXcheck(&ESP01S);
    PCout(13)=1;
    
}

