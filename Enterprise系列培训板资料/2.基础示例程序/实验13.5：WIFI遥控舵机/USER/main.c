#include "sys.h"
#include "delay.h"
#include "usart.h"
#include "LED.h"
#include "KEY.h"
#include "ADC.h"
#include "SERIAL.h"
#include "ESP01S.h"
#include "SERVO.h"
 

/************************************************************
本程序只供学习使用，未经作者许可，不得用于其它任何用途
修改日期:2023/2/24
版本：V4.1
作者：DG_JiuYuan
Gitee个人主页:https://gitee.com/dugu-jiuyuan
All rights reserved
************************************************************/


int main(void)
{
    int i,angle=0,len;
    delay_init();	    //延时函数初始化	
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
    uart_init(115200);
    
    LED0_Init();
    KEY_Init();
    Servo_Init();
    ESP01S_Init();
    
    ESP01S_Connect();
    while(1)
    {
        
        LED0_Flash();
        if(KEY_Read(0)==1)
        {
            ESP01S_DisConnect();
        }
        else if(KEY_Read(1)==1)
        {
            ESP01S_Connect();
        }

/****************************************************************************************
//因为我们手机端的TCP调试APP发送指令的换行符为'\r',而串口通信的截止符为'\r\n'。
//需要前往SERIAL.H文件中将RX_MOOD宏值修改为1
//        
//ESP01S作为TCP 客户端接收到信息的格式为：\r\n+IPD,%d:%s
//%d:接收到的信息长度
//%s:接收到的字符串
//因此，当我们读取到"+IPD,"字符串后，下一位就是字符串长度，这里可以使用字符串处理的方法
//我们约定，手机发送舵机目标角度：0-180
//则接收到的数据应该为"\r\n+IPD,%d:%d\n",len,num
//len:手机发送的字符串的长度，包括了结尾的换行符
//angle:舵机的目标角度
//因此，我们可以利用长度值len处理接收到的字符串，即可得到目标角度     
        
****************************************************************************************/
        
        if(Serial_RXcheck(&ESP01S)!=0)
        {
            len = ESP01S.RX_BUF[6]-'0'-1;//读取接收数据帧第6位，并去掉结尾的回车符，计算角度位数
            for(angle=0,i=8;i<8+len;i++)
            {
                angle=angle*10+ESP01S.RX_BUF[i]-'0';
            }
            printf("len=%d,angle=%d\r\n",len,angle);
            printf("%c%c%c\r\n",ESP01S.RX_BUF[8],ESP01S.RX_BUF[9],ESP01S.RX_BUF[10]);
            Servo_Drive(angle);
        }

    }

}

//ESP01S接收中断，注意需前往SERIAL.h中将原来的USART2中断函数注释掉
void USART2_IRQHandler(void)
{
    Serial_RX_u8str(&ESP01S);
}

