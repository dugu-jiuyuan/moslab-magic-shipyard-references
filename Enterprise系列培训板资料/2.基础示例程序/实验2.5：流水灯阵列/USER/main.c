#include "sys.h"
#include "delay.h"
#include "usart.h"
#include "LED.h"
  
/************************************************************
本程序只供学习使用，未经作者许可，不得用于其它任何用途
修改日期:2023/2/24
版本：V4.1
作者：DG_JiuYuan
Gitee个人主页:https://gitee.com/dugu-jiuyuan
All rights reserved
************************************************************/

int main(void)
{
    delay_init();	    //延时函数初始化	
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
    uart_init(115200);
    
    LED0_Init();
    LEDGroup_Init();//初始化LED阵列
    
    while(1)
    {     
        LED0_Flash();
        LEDGroup_Flash(2);//LED阵列向一个方向流水闪烁
    }

}

