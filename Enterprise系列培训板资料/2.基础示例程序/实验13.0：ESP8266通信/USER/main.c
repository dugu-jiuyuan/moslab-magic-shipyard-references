#include "sys.h"
#include "delay.h"
#include "usart.h"
#include "LED.h"
#include "KEY.h"
#include "ADC.h"
#include "SERIAL.h"
#include "ESP01S.h"
 

/************************************************************
本程序只供学习使用，未经作者许可，不得用于其它任何用途
修改日期:2023/2/24
版本：V4.1
作者：DG_JiuYuan
Gitee个人主页:https://gitee.com/dugu-jiuyuan
All rights reserved
************************************************************/


int main(void)
{
    delay_init();	    //延时函数初始化	
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
    uart_init(115200);
    
    LED0_Init();
    KEY_Init();
    
    ESP01S_Init();
    
    ESP01S_Connect();
    
    while(1)
    {
        LED0_Flash();
        
        if(KEY_Read(0)==1)
        {
            ESP01S_DisConnect();
        }
        else if(KEY_Read(1)==1)
        {
            ESP01S_Connect();
        }
        delay_ms(1000);
        
        ESP01S_Send("EPS01S IS READY\r\n");
        
    }

}

//ESP01S接收中断，注意需前往SERIAL.h中将原来的USART2中断函数注释掉
void USART2_IRQHandler(void)
{
    Serial_RX_u8str(&ESP01S);
}
