#ifndef __U8LIB_H
#define __U8LIB_H

#include "sys.h"

/************************************************************
本程序只供学习使用，未经作者许可，不得用于其它任何用途
作者：DG_JiuYuan
修改日期:2021/8/23
版本：V2.0
All rights reserved
************************************************************/

void u8cpy(u8 *str1,u8 *str2);
u32 u8len(u8 *str);
u8 Get_numlen(u32 num);
u32 u8pow(u8 m, u8 n);
void TurnString(char* Final,char* fmt,...);


#endif

