#include "sys.h"
#include "ESP01S.h"
#include "SERIAL.h"
#include "_u8_Lib.h"
#include "usart.h"
#include "delay.h"

SERIAL ESP01S;


void ESP01S_Init(void)
{
    //根据AT指令手册连接WIFI
    u8 AT1[]="AT+CWMODE=3";
    u8 AT2[]="AT+CWJAP=\"DG_JiuYuan\",\"12345678\"";//输入自己的WIFI名和密码
    
    USART2_Init(115200);
    Serial_Init(&ESP01S, USART2);
    
    Serial_TX_u8str(&ESP01S, AT1);
    delay_ms(1000);
    Serial_TX_u8str(&ESP01S, AT2);
    delay_ms(1000);
    delay_ms(1000);
    delay_ms(1000);
    
}

void ESP01S_Send(u8 *str)
{
    u16 len;
    u8 AT_TX[16];
    len = u8len(str);
    TurnString((char*)AT_TX,"AT+CIPSEND=%d\0",len+2);//发送AT指令，告诉ESPS
    
    Serial_TX_u8str(&ESP01S,AT_TX);
    delay_ms(10);//给联网模块一个缓冲时间
    Serial_TX_u8str(&ESP01S,str);
    
    
}


void ESP01S_Connect(void)
{
    u8 AT_C[]="AT+CIPSTART=\"TCP\",\"192.168.43.1\",8086";
    Serial_TX_u8str(&ESP01S, AT_C);
    
    PCout(13)=0;
    delay_ms(1000);
    Serial_RXcheck(&ESP01S);
    PCout(13)=1;
    
}


void ESP01S_DisConnect(void)
{
    u8 AT_DC[]="AT+CIPCLOSE";
    Serial_TX_u8str(&ESP01S, AT_DC);
    
    PCout(13)=0;
    delay_ms(1000);
    Serial_RXcheck(&ESP01S);
    PCout(13)=1;
    
}
