#include "MOTOR.h"
#include "sys.h"
#include "usart.h"

//将PB0，PB1引脚作为电机的控制引脚，这两个脚同时也是TIM3的通道3，4的引脚，但这里只做普通IO使用
void Motor_Init(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;
 
 	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB,ENABLE);//使能GPIOB时钟

	GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_0|GPIO_Pin_1;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    
 	GPIO_Init(GPIOB, &GPIO_InitStructure);
    
    GPIO_ResetBits(GPIOB, GPIO_Pin_0|GPIO_Pin_1);
    
}


void Motor_Drive(int speed)
{
    printf("speed = %d\r\n",speed);
    
    if(speed > 0)
    {
        //控制电机的引脚
        speed = -speed;
        PBout(0) = 1;
        PBout(1) = 0;
        
        //控制LED阵列，表示电机转动方向
        PBout(12) = 1;
        PBout(13) = 0;
        
    }
    else if(speed < 0)
    {
        //控制电机的引脚
        PBout(0) = 0;
        PBout(1) = 1;
        
        //控制LED阵列，表示电机转动方向
        PBout(12) = 0;
        PBout(13) = 1;
        speed = -speed;
        
    }
    else
    {
        //控制电机的引脚
        PBout(0) = 0;
        PBout(1) = 0;
        
        //控制LED阵列，表示电机转动方向
        PBout(12) = 0;
        PBout(13) = 0;
        
    }
    
    TIM3->CCR1 = speed;
    
}


/**************************************************************************
函数功能：增量PI控制器
入口参数：编码器测量值，目标速度
返回  值：电机PWM
根据增量式离散PID公式 
pwm+=Kp[e（k）-e(k-1)]+Ki*e(k)+Kd[e(k)-2e(k-1)+e(k-2)]
e(k)代表本次偏差 
e(k-1)代表上一次的偏差  以此类推 
pwm代表增量输出
在我们的速度控制闭环系统里面，只使用PI控制
pwm+=Kp[e（k）-e(k-1)]+Ki*e(k)
**************************************************************************/
float Velocity_KP=10,Velocity_KI=10;

int Incremental_PI_MOTOR_CTRL(int Encoder,int Target)
{ 	
    static int Bias,Pwm,Last_bias;
    Bias=-Encoder+Target;                //计算偏差
    Pwm+=Velocity_KP*(Bias-Last_bias)+Velocity_KI*Bias;   //增量式PI控制器
    //输出限幅
    if(Pwm>2000)Pwm=2000;
    if(Pwm<-2000)Pwm=-2000;
    Last_bias=Bias;	                   //保存上一次偏差 
    return Pwm;                         //增量输出
    
}
