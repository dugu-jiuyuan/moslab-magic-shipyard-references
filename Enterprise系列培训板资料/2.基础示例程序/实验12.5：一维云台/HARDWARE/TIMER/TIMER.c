#include "sys.h"
#include "TIMER.h"
#include "LED.h"
#include "ADC.h"
#include "usart.h"


void TIM3_PWM_Init(u16 arr, u16 psc)
{
	GPIO_InitTypeDef GPIO_InitStruct;
	TIM_TimeBaseInitTypeDef TIM_TimeBaseStruct;
	TIM_OCInitTypeDef TIM_OCInitTypeStruct;
	
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3,ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA  ,ENABLE); 
    
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_6;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA,&GPIO_InitStruct);
	
	//初始化TIM4
	TIM_TimeBaseStruct.TIM_Period = arr;//重装载值 
	TIM_TimeBaseStruct.TIM_Prescaler = psc;//预分频值 
	TIM_TimeBaseStruct.TIM_ClockDivision = 0; //时钟分频1、2、4分频	
	TIM_TimeBaseStruct.TIM_CounterMode = TIM_CounterMode_Up;//设置计数模式
	TIM_TimeBaseInit(TIM3,&TIM_TimeBaseStruct);
	
	//初始化输出比较参数
	TIM_OCInitTypeStruct.TIM_OCMode = TIM_OCMode_PWM2; //选择定时器模式
	TIM_OCInitTypeStruct.TIM_OutputState = TIM_OutputState_Enable;//比较输出使能
	TIM_OCInitTypeStruct.TIM_OCPolarity = TIM_OCPolarity_Low;//输出极性
	TIM_OC1Init(TIM3,&TIM_OCInitTypeStruct); //选择定时器输出通道 TIM4_CH1
	
	//使能预装载寄存器
	TIM_OC1PreloadConfig(TIM3,TIM_OCPreload_Enable);
	
	//使能定时器
	TIM_Cmd(TIM3,ENABLE);
}



//void TIM2_Int_Init(u16 psc, u16 arr)
//{
//    TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
//    NVIC_InitTypeDef NVIC_InitStructure;

//    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE); //时钟使能

//    //定时器TIM3初始化
//    TIM_TimeBaseStructure.TIM_Period = arr; //设置在下一个更新事件装入活动的自动重装载寄存器周期的值	
//    TIM_TimeBaseStructure.TIM_Prescaler =psc; //设置用来作为TIMx时钟频率除数的预分频值
//    TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1; //设置时钟分割:TDTS = Tck_tim
//    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;  //TIM向上计数模式
//    TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure); //根据指定的参数初始化TIMx的时间基数单位

//    TIM_ITConfig(TIM2,TIM_IT_Update,ENABLE ); //使能指定的TIM3中断,允许更新中断

//    //中断优先级NVIC设置
//    NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;  //TIM2中断
//    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;  //先占优先级0级
//    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;  //从优先级3级
//    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE; //IRQ通道被使能
//    NVIC_Init(&NVIC_InitStructure);  //初始化NVIC寄存器


//    TIM_Cmd(TIM2, ENABLE);  //使能TIMx	
//}



////定时器2中断服务函数
//void TIM2_IRQHandler(void)
//{                                       
//    u16 adc1_ch0,adc1_ch1;
//    if(TIM2->SR&0X0001)
//    {
//        LEDGroup_Flash(1);
//        adc1_ch0=Read_ADC(0);
//        adc1_ch1=Read_ADC(1);
//        printf("adc1_ch0=%d,adc1_ch1=%d\r\n",adc1_ch0,adc1_ch1);
//        
//    }                   
//    TIM2->SR&=~(1<<0);//清除中断标志位
//}


