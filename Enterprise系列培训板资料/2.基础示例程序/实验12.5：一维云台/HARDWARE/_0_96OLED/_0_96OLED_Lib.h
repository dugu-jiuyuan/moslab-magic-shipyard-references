#ifndef __0_96OLEDLIB_H
#define __0_96OLEDLIB_H
#include "sys.h"

typedef struct 
{
    u16 SDA_pin;
    u16 SCK_pin;
    GPIO_TypeDef* SDA_GPIOx;
    GPIO_TypeDef* SCK_GPIOx;
    u8 Screen_GRAM[144][8];

}SCREEN;

#define SCREEN_CMD  0
#define SCREEN_DATA 1

//#define SCREEN_SCLK_Clr(sc) ((sc)->SCK_GPIOx->BRR)=((sc)->SCK_pin)
//#define SCREEN_SCLK_Set(sc) ((sc)->SCK_GPIOx->BSRR)=((sc)->SCK_pin)

//#define SCREEN_SDIN_Clr(sc) ((sc)->SDA_GPIOx->BRR)=((sc)->SDA_pin)
//#define SCREEN_SDIN_Set(sc) ((sc)->SDA_GPIOx->BSRR)=((sc)->SDA_pin)

#define SCREEN_SCLK_Clr(sc) GPIO_ResetBits((sc)->SCK_GPIOx,(sc)->SCK_pin)
#define SCREEN_SCLK_Set(sc) GPIO_SetBits((sc)->SCK_GPIOx,(sc)->SCK_pin)

#define SCREEN_SDIN_Clr(sc) GPIO_ResetBits((sc)->SDA_GPIOx,(sc)->SDA_pin)
#define SCREEN_SDIN_Set(sc) GPIO_SetBits((sc)->SDA_GPIOx,(sc)->SDA_pin)


//void SCREEN_SCLK_Clr(SCREEN *Screen);
//void SCREEN_SCLK_Set(SCREEN *Screen);
//void SCREEN_SDIN_Clr(SCREEN *Screen);
//void SCREEN_SDIN_Set(SCREEN *Screen);

void Screen_Init(SCREEN* Screen,char SCK_GPIOx,u16 SCK_pin,char SDA_GPIOx,u16 SDA_pin);

void Screen_I2C_Start(SCREEN* Screen);
void Screen_I2C_Stop(SCREEN* Screen);
void Screen_I2C_WaitAck(SCREEN* Screen);

void Screen_Send_Byte(SCREEN* Screen, u8 dat);
void Screen_WR_Byte(SCREEN* Screen, u8 dat,u8 mode);
void Screen_WR_BP(SCREEN* Screen, u8 x,u8 y);

void Screen_DisPlay_On(SCREEN* Screen);
void Screen_DisPlay_Off(SCREEN* Screen);
void Screen_Refresh(SCREEN* Screen);

void Screen_DrawPoint(SCREEN* Screen, u8 x,u8 y);
void Screen_ClearPoint(SCREEN* Screen, u8 x,u8 y);
void Screen_ColorTurn(SCREEN* Screen, u8 i);
void Screen_DisplayTurn(SCREEN* Screen, u8 i);
void Screen_Clear(SCREEN* Screen);
void Screen_DrawLine(SCREEN* Screen, u8 x1,u8 y1,u8 x2,u8 y2);
void Screen_DrawCircle(SCREEN* Screen, u8 x,u8 y,u8 r);

void Screen_ShowChar(SCREEN* Screen, u8 x,u8 y,u8 chr,u8 size1);
void Screen_ShowBackChar(SCREEN* Screen, u8 x,u8 y,u8 chr,u8 size1);

void Screen_ShowString(SCREEN* Screen, u8 x,u8 y,u8 *chr,u8 size1);
void Screen_ShowBackString(SCREEN* Screen, u8 x,u8 y,u8 *chr,u8 size1);

void Screen_ShowNum(SCREEN* Screen, u8 x,u8 y,u32 num,u8 len,u8 size1);
void Screen_ShowBackNum(SCREEN* Screen, u8 x,u8 y,u32 num,u8 len,u8 size1);

void Screen_ShowNum1(SCREEN* Screen, u16 x,u16 y,float num,u8 len,u8 size1);
void Screen_ShowBackNum1(SCREEN* Screen, u16 x,u16 y,float num,u8 len,u8 size1);

void Screen_ShowInt(SCREEN* Screen, u8 x,u8 y,int num,u8 len,u8 size1);
void Screen_ShowBackInt(SCREEN* Screen, u8 x,u8 y,int num,u8 len,u8 size1);

void Screen_ShowFloat(SCREEN* Screen, u16 x,u16 y,float num,u8 len,u8 size1);
void Screen_ShowBackFloat(SCREEN* Screen, u16 x,u16 y,float num,u8 len,u8 size1);

void Screen_ShowIntTag(SCREEN* Screen, u16 x,u16 y,int num,int len,u8 size1,u8 head);
void Screen_ShowFloatTag(SCREEN* Screen, u16 x,u16 y,float num,int len,u8 size1,u8 head);

void Screen_ShowChinese(SCREEN* Screen, u8 x,u8 y,u8 num,u8 size1);
void Screen_ScrollDisplay(SCREEN* Screen, u8 num,u8 space);





#endif // !__0_96OLEDLIB_H
