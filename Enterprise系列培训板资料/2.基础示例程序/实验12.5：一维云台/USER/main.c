#include "sys.h"
#include "delay.h"
#include "usart.h"
#include "LED.h"
#include "KEY.h"
#include "ADC.h"
#include "TIMER.h"
#include "MPU6050.h"
#include "_0_96OLED_Lib.h"
#include "_u8_Lib.h"
#include "inv_mpu.h"
#include "inv_mpu_dmp_motion_driver.h" 
#include "SERVO.h"
 
 
/************************************************************
本程序只供学习使用，未经作者许可，不得用于其它任何用途
修改日期:2023/2/24
版本：V4.1
作者：DG_JiuYuan
Gitee个人主页:https://gitee.com/dugu-jiuyuan
All rights reserved
************************************************************/


SCREEN Screen;//由于MPU6050的DMP功能会占用较大的堆栈，因此一定要把显示屏结构体扔在主函数之外，否则会使的堆栈溢出

int main(void)
{
	float pitch,roll,yaw; 		//欧拉角
    int angle;
    u8 pitch_str[16],roll_str[16],yaw_str[16];
    
    delay_init();	    //延时函数初始化	
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
    uart_init(115200);
    
    LED0_Init();
    Screen_Init(&Screen,'A',11,'A',12);
    MPU_Init();//初始化陀螺仪本体
    Servo_Init();
    
    
    while(mpu_dmp_init())
 	{
		printf("MPU6050 DMP ERROR\r\n");
 		delay_ms(200);
	}  //等待DMP功能成功初始化
    
    
    printf("MPU6050 OK\r\n");
 	while(1)
	{
		if(mpu_dmp_get_data(&pitch,&roll,&yaw)==0)
		{ 
            printf("pitch=%.2f, roll=%.2f, yaw=%.2f\r\n",pitch, roll, yaw);
            printf("***************************************************\r\n\r\n");
            
            TurnString((char*)pitch_str,"pitch=%.2f  ",pitch);
            TurnString((char*)yaw_str,"yaw=%.2f  ",yaw);
            TurnString((char*)roll_str,"roll=%.2f  ",roll);
            
            Screen_ShowString(&Screen,0,0,pitch_str,16);
            Screen_ShowString(&Screen,0,20,yaw_str,16);
            Screen_ShowString(&Screen,0,40,roll_str,16);
            
            Screen_Refresh(&Screen);
            
            angle = roll+90;
            
            Servo_Drive(angle);
            
        }	
      
    }

}
